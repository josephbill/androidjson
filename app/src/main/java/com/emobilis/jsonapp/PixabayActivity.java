package com.emobilis.jsonapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emobilis.jsonapp.adapter.PixabayAdapter;
import com.emobilis.jsonapp.model.PixabayModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PixabayActivity extends AppCompatActivity {
    //declare my views
    RecyclerView recyclerView;
    //request queue from volley
    RequestQueue requestQueue;
    //adapter
    PixabayAdapter pixabayAdapter;
    //model
    ArrayList<PixabayModel> pixabayModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pixabay);

        //ref to volley singleton
        requestQueue = VolleySingelton.getInstance(this).getRequestQueue();

        //ref to our recyclerview
        recyclerView = findViewById(R.id.recyclerCats);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //intialize my arraylist
        pixabayModels = new ArrayList<>();

        //method to fetch my JSON data
        jsonParse();


    }
    //using volley to fetch data
    private void jsonParse() {
        String url = "https://pixabay.com/api/?key=5303976-fd6581ad4ac165d1b75cc15b3&q=kitten&image_type=photo&pretty=true";

        //creating a new JSON request
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("hits");
                    //for loop to iterate over the objects
                    for (int i=0; i < jsonArray.length(); i++){
                        JSONObject cats = jsonArray.getJSONObject(i);
                        //pick my items
                        String creatorName = cats.getString("user");
                        String imageURL = cats.getString("webformatURL");
                        int likes = cats.getInt("likes");
                        //type casting
                        String like = String.valueOf(likes);

                        //i need to add the info , to the model class
                        pixabayModels.add(new PixabayModel(imageURL,creatorName,like));
                    }
                    //add this model to the adapter according to the adapters constructor
                    pixabayAdapter = new PixabayAdapter(PixabayActivity.this,pixabayModels);
                    //set this adapter to the recyclerview
                    recyclerView.setAdapter(pixabayAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PixabayActivity.this, "Data was not fetched, check connection", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }
}
