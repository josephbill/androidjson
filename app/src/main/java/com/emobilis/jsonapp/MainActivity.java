package com.emobilis.jsonapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    //declare elements
    TextView text_view_result;
    //declare RequestQue
    RequestQueue requestQueue;
    Button btnJson,button_post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ref
        text_view_result = findViewById(R.id.text_view_result);
        btnJson = findViewById(R.id.button_parse);
        button_post = findViewById(R.id.button_post);


        //create the reference to the singleton class
        requestQueue = VolleySingelton.getInstance(this).getRequestQueue();

        //on click
        btnJson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jsonParse();
            }
        });

        button_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,PostVolleyActivity.class);
                startActivity(intent);
            }
        });




    }





    //here we will consume json data using volley
    private void jsonParse() {
        //define the base url
        String URL = "https://cat-fact.herokuapp.com/facts";

        //create a new instance of the JSONOBJECTREQUEST CLASS FROM VOLLEY
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            //i reference the array
                            JSONArray jsonArray = response.getJSONArray("all");
                            //for loop to iterate over the array
                            for (int i = 0; i < jsonArray.length(); i++){
                                //come and fetch objects with array
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                //use my json object to refer to value in my json response
                                //have a variables to store the info
                                String id = jsonObject.getString("_id");
                                String text = jsonObject.getString("text");
                                String type = jsonObject.getString("type");
                                text_view_result.append(id + ", " + text);
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
               @Override
            public void onErrorResponse(VolleyError error) {
                   error.printStackTrace();
                   Toast.makeText(MainActivity.this, "Something went wrong with fetching records", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }

    public void button_display(View v){
        Intent intent = new Intent(MainActivity.this,PixabayActivity.class);
        startActivity(intent);
    }
}
