package com.emobilis.jsonapp;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.lang.ref.ReferenceQueue;

public class VolleySingelton {
     //declare the instance of this file
    private static VolleySingelton volleySingelton;
    private RequestQueue requestQueue;

    //constructor to be used by other activity requesting network service
    private VolleySingelton(Context context){
         //create the new instance
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    //
    public static synchronized VolleySingelton getInstance(Context context){
        //we check if the activity requesting this file has volley instance attached to it
        if (volleySingelton == null){
            volleySingelton = new VolleySingelton(context);
        }

        return volleySingelton;
    }

    //create the instance of the request queue
    public RequestQueue getRequestQueue(){
        return requestQueue;
    }

}
