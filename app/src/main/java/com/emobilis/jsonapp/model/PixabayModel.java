package com.emobilis.jsonapp.model;

public class PixabayModel {
    //define variables
    private String imageURL;
    private String creatorName;
    private String likes;

    //define our constructor
    public PixabayModel(String imageURL,String creatorName,String likes){
        this.imageURL = imageURL;
        this.creatorName = creatorName;
        this.likes = likes;
    }

    //get and set

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}
