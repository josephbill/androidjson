package com.emobilis.jsonapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

public class PostVolleyActivity extends AppCompatActivity {
   //declare views
    EditText name,second,email;
    Button btnSubmit;
    //RequestQueue
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_volley);

        //ref
        name = findViewById(R.id.name);
        second = findViewById(R.id.secondname);
        email = findViewById(R.id.email);
        btnSubmit = findViewById(R.id.btnSubmit);

        //volley singleton instance
        requestQueue = VolleySingelton.getInstance(this).getRequestQueue();

        //onclick
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call method to post
                postData();
            }
        });
    }

    private void postData() {
        //pick users input
        String name_of_user = name.getText().toString().trim();
        String second_name = second.getText().toString().trim();
        //now we can code volley
        String postUrl = "https://reqres.in/api/users";

        //create new JSONOBJECT
        JSONObject postData = new JSONObject();
        //add to your url
        try{
            postData.put("name",name_of_user);
            postData.put("job",second_name);

        }catch (Exception e){
            e.printStackTrace();
        }

        //now i can create the volley listeners for my response and if there is a failure
        //create new instance of the JSONObjectRequest
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, postUrl, postData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(PostVolleyActivity.this, "Data Posted Successfully", Toast.LENGTH_SHORT).show();
                Toast.makeText(PostVolleyActivity.this, response.toString(), Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PostVolleyActivity.this, "Error occurred, check your connection", Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonObjectRequest);
    }


}
