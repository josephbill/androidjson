package com.emobilis.jsonapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.emobilis.jsonapp.R;
import com.emobilis.jsonapp.model.PixabayModel;

import java.util.ArrayList;

public class PixabayAdapter extends RecyclerView.Adapter<PixabayAdapter.PixabayViewHolder> {
    //define variables
    private Context context;
    //define the data list
    private ArrayList<PixabayModel> pixabayModels;

    //constructor
    public PixabayAdapter(Context context, ArrayList<PixabayModel> pixabayModels1){
        this.context = context;
        this.pixabayModels = pixabayModels1;
    }


    @NonNull
    @Override
    public PixabayAdapter.PixabayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.cat_item, parent, false);
        return new PixabayViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PixabayAdapter.PixabayViewHolder holder, int position) {
        PixabayModel pixabayModel = pixabayModels.get(position);
        //reference to get my data and populate my views
        holder.name.setText(pixabayModel.getCreatorName());
        holder.likes.setText(pixabayModel.getLikes());
        //glide to display my image
        Glide.with(context).load(pixabayModel.getImageURL()).into(holder.imageCat);
    }

    @Override
    public int getItemCount() {
        return pixabayModels.size();
    }

    //view holder
    public class PixabayViewHolder extends RecyclerView.ViewHolder {
        //define the view within item
        TextView name,likes;
        ImageView imageCat;

        public PixabayViewHolder(@NonNull View itemView) {
            super(itemView);
            //find ref
            name = itemView.findViewById(R.id.creatorName);
            likes = itemView.findViewById(R.id.textLikes);
            imageCat = itemView.findViewById(R.id.imageName);
        }
    }

}
